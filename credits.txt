The menu background used is "Sunrise Through Mist" by Fitz Hugh Lane.
It has been obtained thanks to "The Athenaeum" ( http://the-athenaeum.org/ ).
More details on the said painting can be found at http://the-athenaeum.org/art/detail.php?ID=13984 .


The font Essays 1743 has been created and distributed under the GNU GPL license by John Stracke.
Visit http://www.thibault.org/fonts/essays/ for more information.
